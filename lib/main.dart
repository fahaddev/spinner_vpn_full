import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    bool _value = true;
    return Scaffold(
      backgroundColor: Colors.blue[500],
      // appBar: AppBar(),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 35.0,
              left: 10,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                // Drawer(),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    '',
                    style: TextStyle(fontSize: 27, fontWeight: FontWeight.w900),
                  ),
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    child: Row(
                      children: [
                        // IconButton(
                        //   icon: Icon(
                        //     Icons.favorite_border,
                        //   ),
                        //   onPressed: () {
                        //     // Navigator.of(context).push(MaterialPageRoute(
                        //     //     builder: (context) => Grid()));
                        //   },
                        // ),
                        IconButton(
                            icon: Icon(Icons.replay_rounded), onPressed: () {}),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Text('Logo Section'),
          SizedBox(
            height: 40,
          ),
          Text(
            'BMW Vpn',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 85.0),
                  child: Text('Expires In',
                      style: TextStyle(
                          color: Colors.red, fontWeight: FontWeight.w500)),
                ),
                SizedBox(
                  width: 50,
                ),
                Text('354 Days',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 50.0, right: 50),
            child: Divider(thickness: 4),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40.0, right: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Download',
                      //style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                    ),
                    Spacer(),
                    Text(
                      'Upload',
                      //style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 900,
                  color: Colors.green,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Column(
                        children: [
                          Text(
                            '5.9 KB',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          Text(
                            '43.8 kbit',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ],
                      ),
                      Spacer(),
                      Column(
                        children: [
                          Text(
                            '5.9 KB',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          Text(
                            '43.8 kbit',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40.0, right: 50, bottom: 15),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(60),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 2.0,
                      blurRadius: 5.0,
                    ),
                  ]),
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Icon(Icons.vpn_key_sharp),
                    ),
                    labelText: '   Enter Pin',
                    suffixIcon: Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: Icon(Icons.remove_red_eye),
                    )),
              ),
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 85.0),
                child: Checkbox(
                  value: _value,
                  onChanged: (bool newValue) {
                    setState(() {
                      _value = newValue;
                    });
                  },
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text('Remember Me')
            ],
          ),
          FlatButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
            // elevation: 5,
            onPressed: () {},
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 20, bottom: 20, left: 60, right: 60),
              child: Text(
                'Connect',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
            color: Colors.blue[900],
          ),
          SizedBox(
            height: 20,
          ),
          FlatButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
            // elevation: 5,
            onPressed: () {},
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 20, bottom: 20, left: 35, right: 35),
              child: Text(
                'Reload Settings',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
            color: Colors.green,
          ),
          SizedBox(
            height: 80,
          ),
          Text('Version 1.0'),
        ],
      ),
    );
  }
}
